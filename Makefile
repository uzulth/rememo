include ./.env
export

# Setup —————————————————————————————————————————————————————————————————————————
EXEC_PHP       = php
GIT            = git
SF_ENV        ?= dev
DOCKER_PHP     = docker exec --tty ${PHP_CONTAINER_NAME}
DOCKER_COMPOSE = docker-compose
SYMFONY        = $(DOCKER_PHP) $(EXEC_PHP) admin/bin/console
COMPOSER       = $(DOCKER_PHP) composer

help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z_\.@-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | sed -e 's/Makefile://' | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## ------------------- Composer 🐘 -------------------
install: ./admin/composer.lock ## Install vendors according to the current composer.lock file
	COMPOSER_MEMORY_LIMIT=-1 $(COMPOSER) install --no-scripts

update: composer.json ## Update vendors according to the composer.json file
	COMPOSER_MEMORY_LIMIT=-1 $(COMPOSER) update

## ------------------- Symfony BackEnd 🎵 -------------------
sf: ## 💡 List Symfony commands
	$(SYMFONY)

init-db: ## Doctrine schema update
	$(SYMFONY) doctrine:schema:update --force

migration: ## Doctrine schema update
	$(SYMFONY) make:migration
	$(SYMFONY) doctrine:migrations:migrate

cc: ## Clear/warmup cache and build api cache file
	$(SYMFONY) cache:clear --env=$(SF_ENV)

fixtures: ## Load fixtures
	$(SYMFONY) doctrine:fixtures:load --no-interaction

tests: ## Run units tests
	./admin/vendor/bin/simple-phpunit --coverage-text --colors

build-admin: install

## ------------------- Docker ENV 🐳 ---------------------
docker-up: ## Start the docker
	docker-compose up -d

docker-stop: ## Start the docker
	docker-compose stop

docker-build:  ## Build the docker
	docker-compose up -d --build --remove-orphans

bash-php: ## Connect into PHP container
	docker exec -ti flashcards_php bash

bash-nginx: ## Connect into Nginx container
	docker exec -ti flashcards_nginx bash

bash-db: ## Connect into DB container
	docker exec -ti flashcards_db bash

logs-php: ## ✏️  Show logs of the php container
	${DOCKER_COMPOSE} logs --tail="100" -f php

logs-nginx: ## ✏️  Show logs of the nginx container
	${DOCKER_COMPOSE} logs --tail="100" nginx