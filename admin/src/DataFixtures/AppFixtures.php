<?php

namespace App\DataFixtures;

use App\Entity\Deck;
use App\Entity\Flashcard;
use App\Entity\Tag;
use App\Entity\User;
use App\Manager\TagManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /** 
     * @var TagManager $tagManager
     */
    private $tagManager;

    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        TagManager $tagManager
    ) {
        $this->passwordEncoder = $passwordEncoder;
        $this->tagManager      = $tagManager;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();

        $listOfTags = [
            'JavaScript',
            'PHP',
            'Cuisine',
            'Mathématique'
        ];

        // create user admin
        $admin = (new User())
            ->setLastName('admin')
            ->setFirstName('admin')
            ->setEmail('admin@admin.com')
            ->setBlocked(false)
        ;

        $admin->setPassword(
            $this->passwordEncoder->encodePassword($admin, 'admin')
        );

        $manager->persist($admin);

        // Create Tag
        foreach ($listOfTags as $tag) {
            $tag = (new Tag())
                ->setName($tag);

            $manager->persist($tag);
        }

        $manager->flush();

        // Create User
        for ($u = 0; $u < 5; $u++) {
            $user = (new User())
                ->setFirstName($faker->firstname())
                ->setLastName($faker->lastname())
                ->setEmail($faker->email())
                ->setBlocked(false)
            ;

            $user->setPassword(
                $this->passwordEncoder->encodePassword($user, 'test')
            );

            $manager->persist($user);

            // Create Deck
            for ($d = 0; $d < 4; $d++) {

                $deck = (new Deck())
                    ->setName(sprintf('Deck name #', $d))
                    ->setCreatedBy($user)
                    ->setTag($this->tagManager->findRandom());

                for ($f = 0; $f < 5; $f++) {
                    // Create Flashcard
                    $flashcard = (new Flashcard())
                        ->setName(sprintf('Flashcard name #%s', $f))
                        ->setQuestion(sprintf('(#%s) Question lorem ipsum', $f))
                        ->setAnswer(sprintf('(#%s) Answer lorem ipsum', $f))
                        ->setDeck($deck)
                        ->setCreatedBy($user);

                    $manager->persist($flashcard);
                }

                $manager->persist($deck);
            }
        }

        $manager->flush();
    }
}
