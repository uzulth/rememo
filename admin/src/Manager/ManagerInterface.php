<?php

namespace App\Manager;

interface ManagerInterface
{
    /**
     * @param $id
     * @return void
     */
    public function delete($id): void;

    /**
     * @param $id
     * @return mixed
     */
    public function find($id);

    /**
     * @return mixed
     */
    public function findAll();

    /**
     * @param $entity
     */
    public function insert($entity): void;

    /**
     * @param $entity
     */
    public function update($entity): void;
}
