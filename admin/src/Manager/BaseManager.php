<?php

namespace App\Manager;

use Doctrine\Persistence\ObjectManager;

class BaseManager
{
    /**
     * @var ObjectManager $manager
     */
    protected $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    protected function remove($entity): void
    {
        $this->manager->remove($entity);
        $this->manager->flush();
    }

    public function persist($entity): void
    {
        $this->manager->persist($entity);
        $this->manager->flush();
    }
}