<?php

namespace App\Manager;

use App\Entity\Tag;
use App\Manager\BaseManager;
use App\Repository\TagRepository;
use Doctrine\Persistence\ObjectManager;

class TagManager extends BaseManager
{
    /**
     * @var TagRepository $repository
     */
    private $repository;

    public function __construct(TagRepository $repository, ObjectManager $manager)
    {
        $this->repository = $repository;
        parent::__construct($manager);
    }

    public function find($id): ?Tag
    {
        return $this->repository->find($id);
    }

    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function findRandom()
    {
        $entities = $this->findAll();
        shuffle($entities);

        return $entities[0];
    }
}
