<?php

namespace App\Manager;

use App\Entity\Deck;
use App\Entity\Evaluation;
use App\Entity\Flashcard;
use App\Entity\User;
use App\Manager\BaseManager;
use App\Repository\EvaluationRepository;
use Doctrine\Persistence\ObjectManager;

class EvaluationManager extends BaseManager
{
    /**
     * @var EvaluationRepository $repository
     */
    private $repository;

    public function __construct(EvaluationRepository $repository, ObjectManager $manager)
    {
        $this->repository = $repository;
        parent::__construct($manager);
    }

    public function find($id): ?Evaluation
    {
        return $this->repository->find($id);
    }

    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function findRandom()
    {
        $entities = $this->findAll();
        shuffle($entities);

        return $entities[0];
    }

    /**
     * Check if evaluation already exist or create it
     *
     * @param Deck $deck
     * @param User $user
     * @return Evaluation
     */
    public function findOrCreateEvaluationFromDeck(Deck $deck, User $user): Evaluation
    {
        $evaluation = $this->repository->findOneBy([
            'deck' => $deck->getId(),
            'user' => $user->getId(),
        ]);

        if (empty($evaluation)) {
            $evaluation = (new Evaluation())
                ->setUser($user)
                ->setDeck($deck)
            ;

            $this->persist($evaluation);
        }

        return $evaluation;
    }
}
