<?php

namespace App\Manager;

use App\Entity\Deck;
use App\Manager\BaseManager;
use App\Repository\DeckRepository;
use Doctrine\Persistence\ObjectManager;

class DeckManager extends BaseManager
{
    /**
     * @var DeckRepository $repository
     */
    private $repository;

    public function __construct(DeckRepository $repository, ObjectManager $manager)
    {
        $this->repository = $repository;
        parent::__construct($manager);
    }

    public function find($id): ?Deck
    {
        return $this->repository->find($id);
    }

    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function findRandom(): ?Deck
    {
        $entities = $this->findAll();
        shuffle($entities);

        return $entities[0];
    }

    public function findByTag(int $tagId): array
    {
        $entities = $this->repository->findBy(['tag' => $tagId]);

        return $entities;
    }

    public function findByCreator(int $creatorId): array
    {
        $entities = $this->repository->findBy(['createdBy' => $creatorId]);

        return $entities;
    }
}
