<?php

namespace App\Manager;

use App\Entity\Flashcard;
use App\Manager\BaseManager;
use App\Repository\FlashcardRepository;
use Doctrine\Persistence\ObjectManager;

class FlashcardManager extends BaseManager
{
    /**
     * @var FlashcardRepository $repository
     */
    private $repository;

    public function __construct(FlashcardRepository $repository, ObjectManager $manager)
    {
        $this->repository = $repository;
        parent::__construct($manager);
    }

    public function find($id): ?Flashcard
    {
        return $this->repository->find($id);
    }

    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function findRandom()
    {
        $entities = $this->findAll();
        shuffle($entities);

        return $entities[0];
    }
}
