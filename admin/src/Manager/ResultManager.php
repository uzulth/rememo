<?php

namespace App\Manager;

use App\Entity\Evaluation;
use App\Entity\Flashcard;
use App\Entity\Result;
use App\Manager\BaseManager;
use App\Repository\ResultRepository;
use Doctrine\Persistence\ObjectManager;

class ResultManager extends BaseManager
{
    /**
     * @var ResultRepository $repository
     */
    private $repository;

    public function __construct(ResultRepository $repository, ObjectManager $manager)
    {
        $this->repository = $repository;
        parent::__construct($manager);
    }

    public function find($id): ?Result
    {
        return $this->repository->find($id);
    }

    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function findRandom()
    {
        $entities = $this->findAll();
        shuffle($entities);

        return $entities[0];
    }

    /**
     * Check if flashcard has already answer for that evaluation
     *
     * @param string $deckId
     * @param Flashcard $flashcard
     * @return bool
     */
    public function flashcardAlreadyAnswer(Evaluation $evaluation, Flashcard $flashcard): bool
    {
        $result = $this->repository->findOneBy([
            'flashcard' => $flashcard,
            'evaluation' => $evaluation
        ]);

        return $result !== null;
    }
}
