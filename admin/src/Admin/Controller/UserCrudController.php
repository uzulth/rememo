<?php

namespace App\Admin\Controller;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_ADD_ANOTHER);
    }

    public function configureFields(string $pageName): iterable
    {
        $id          = IdField::new('Id');
        $firstName   = TextField::new('firstName');
        $lastName    = TextField::new('lastName');
        $email       = EmailField::new('email');
        $password    = TextField::new('password');
        $roles       = ArrayField::new('roles');
        $blocked     = BooleanField::new('blocked');
        $evaluations = CollectionField::new('evaluations');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $firstName, $lastName, $blocked, $roles];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $firstName, $lastName, $email, $evaluations, $blocked, $roles, $password];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$firstName, $lastName, $email, $password, $blocked, $roles];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$firstName, $lastName, $email, $password, $blocked, $roles];
        }
    }
}
