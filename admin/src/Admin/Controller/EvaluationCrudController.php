<?php

namespace App\Admin\Controller;

use App\Entity\Evaluation;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;

class EvaluationCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Evaluation::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_ADD_ANOTHER);
    }

    public function configureFields(string $pageName): iterable
    {
        $id    = IdField::new('id');
        $user  = AssociationField::new('user');
        $deck = AssociationField::new('deck');
        $results = AssociationField::new('results');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $user, $deck];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $user, $deck, $results];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$user];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$user, $deck, $results];
        }
    }
}
