<?php

namespace App\Admin\Controller;

use App\Entity\Deck;
use App\Entity\Evaluation;
use App\Entity\Flashcard;
use App\Entity\Tag;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/", name="admin_dashboard")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('App');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::section('Flashcard');
        yield MenuItem::linkToCrud('Flashcard', 'fa fa-file-text', Flashcard::class);
        yield MenuItem::linkToCrud('Deck', 'fa fa-box', Deck::class);
        yield MenuItem::linkToCrud('Tag', 'fa fa-tags', Tag::class);
        yield MenuItem::linkToCrud('Evaluation', 'fa fa-tags', Evaluation::class);
        yield MenuItem::section('User');
        yield MenuItem::linkToCrud('User', 'fa fa-user', User::class);

        /* Link for exit impersonation */
        // yield MenuItem::linkToExitImpersonation('Stop impersonation', 'fa fa-sign-out-alt');

        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }
}
