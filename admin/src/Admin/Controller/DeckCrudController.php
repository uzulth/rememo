<?php

namespace App\Admin\Controller;

use App\Entity\Deck;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;

class DeckCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Deck::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_ADD_ANOTHER);
    }

    public function configureFields(string $pageName): iterable
    {
        $id         = IdField::new('id');
        $name       = TextField::new('name');
        $tag        = AssociationField::new('tag');
        $flashcards = AssociationField::new('flashcards');
        $createdBy  = AssociationField::new('createdBy');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name, $tag, $flashcards, $createdBy];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $name, $tag, $flashcards, $createdBy];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$name, $tag, $flashcards, $createdBy];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$name, $tag, $flashcards, $createdBy];
        }
    }
}
