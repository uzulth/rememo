<?php

namespace App\Admin\Controller;

use App\Entity\Flashcard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

class FlashcardCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Flashcard::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_ADD_ANOTHER);
    }

    public function configureFields(string $pageName): iterable
    {
        $id        = IdField::new('id');
        $name      = TextField::new('name');
        $question  = TextEditorField::new('question');
        $answer    = TextEditorField::new('answer');
        $deck      = AssociationField::new('deck');
        $createdBy = AssociationField::new('createdBy');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name, $question, $deck, $createdBy];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $name, $question, $answer, $deck, $createdBy];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$name, $question, $answer, $deck, $createdBy];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$name, $question, $answer, $deck, $createdBy];
        }
    }
}
