<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210212110643 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE deck ADD created_by_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE deck ADD CONSTRAINT FK_4FAC3637B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_4FAC3637B03A8386 ON deck (created_by_id)');
        $this->addSql('ALTER TABLE flashcard ADD created_by_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE flashcard ADD CONSTRAINT FK_70511A09B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_70511A09B03A8386 ON flashcard (created_by_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE deck DROP FOREIGN KEY FK_4FAC3637B03A8386');
        $this->addSql('DROP INDEX IDX_4FAC3637B03A8386 ON deck');
        $this->addSql('ALTER TABLE deck DROP created_by_id');
        $this->addSql('ALTER TABLE flashcard DROP FOREIGN KEY FK_70511A09B03A8386');
        $this->addSql('DROP INDEX IDX_70511A09B03A8386 ON flashcard');
        $this->addSql('ALTER TABLE flashcard DROP created_by_id');
    }
}
