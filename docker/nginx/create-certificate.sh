#!/usr/bin/env sh

mkdir /etc/nginx/ssl 2>/dev/null

PATH_SSL="/etc/nginx/ssl"
PATH_CNF="${PATH_SSL}/flashcards.local.cnf"
PATH_KEY="${PATH_SSL}/flashcards.local.key"
PATH_CRT="${PATH_SSL}/flashcards.local.crt"

# Only generate a certificate if there isn't one already there.
if [ ! -f $PATH_CNF ] || [ ! -f $PATH_KEY ] || [ ! -f $PATH_CRT ]
then

    # Uncomment the global 'copy_extentions' OpenSSL option to ensure the SANs are copied into the certificate.
    sed -i '/copy_extensions\ =\ copy/s/^#\ //g' /etc/ssl/openssl.cnf

    # Generate an OpenSSL configuration file specifically for this certificate.
    block="
        [ req ]
        prompt = no
        default_bits = 2048
        default_keyfile = $PATH_KEY
        encrypt_key = no
        default_md = sha256
        distinguished_name = req_distinguished_name
        x509_extensions = v3_ca

        [ req_distinguished_name ]
        O=dnx
        C=UN
        CN=flashcards.local

        [ v3_ca ]
        basicConstraints=CA:FALSE
        subjectKeyIdentifier=hash
        authorityKeyIdentifier=keyid,issuer
        keyUsage = nonRepudiation, digitalSignature, keyEncipherment
        subjectAltName = @alternate_names

        [ alternate_names ]
        DNS.1 = flashcards.local
        DNS.2 = *.flashcards.local
    "
    echo "$block" > $PATH_CNF

    # Finally, generate the private key and certificate.
    openssl genrsa -out "$PATH_KEY" 2048 2>/dev/null
    openssl req -new -x509 -config "$PATH_CNF" -out "$PATH_CRT" -days 365 2>/dev/null
fi

# Handle environment variables substitution
sh /docker-entrypoint.d/20-envsubst-on-templates.sh

nginx
